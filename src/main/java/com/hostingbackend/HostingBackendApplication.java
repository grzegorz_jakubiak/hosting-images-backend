package com.hostingbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HostingBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(HostingBackendApplication.class, args);
    }

}
