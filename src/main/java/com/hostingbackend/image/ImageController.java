package com.hostingbackend.image;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/images")
@AllArgsConstructor
class ImageController {

    private final ImageRepository imageRepository;

    @GetMapping(params = {"!sort", "!page", "!size"})
    ResponseEntity<List<Image>> getImages() {
        List<Image> images = imageRepository.findAll();
        if (Optional.ofNullable(images).isPresent()) {
            return ResponseEntity.ok(images);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    ResponseEntity<List<Image>> getImages(Pageable pageable) {
        List<Image> images = imageRepository.findAll(pageable).getContent();
        if (Optional.ofNullable(images).isPresent()) {
            return ResponseEntity.ok(images);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    ResponseEntity<Image> getImage(@PathVariable int id) {
        return imageRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    ResponseEntity<Image> createImage(@RequestBody Image image) {
        Image savedImage = imageRepository.save(image);
        return ResponseEntity.created(URI.create("/" + savedImage.getId())).body(savedImage);
    }

    @PutMapping("/{id}")
    ResponseEntity<Image> updateImage(@PathVariable int id, @RequestBody Image image) {
        if (imageRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        imageRepository.findById(id)
                .ifPresent(imageToSave -> {
                    imageRepository.save(imageToSave);
                });
        return ResponseEntity.noContent().build();

    }

    @DeleteMapping("/{id}")
    ResponseEntity<Image> postImage(@PathVariable int id, @RequestBody Image image) {
        if (imageRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();

    }
}
