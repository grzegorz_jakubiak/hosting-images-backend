package com.hostingbackend.image;

import java.util.List;
import java.util.Optional;

interface ImageGroupRepository {
    List<ImageGroup> findAll();
    Optional<ImageGroup> findById(Integer id);
    ImageGroup save(ImageGroup entity);
    void deleteById(Integer id);
}
