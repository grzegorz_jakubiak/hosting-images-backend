package com.hostingbackend.image;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/image_groups")
@AllArgsConstructor
public class ImageGroupController {

    private final ImageGroupRepository imageGroupRepository;

    @GetMapping(params = {"!sort", "!page", "!size"})
    ResponseEntity<List<ImageGroup>> getImageGroups() {
        List<ImageGroup> imageGroups = imageGroupRepository.findAll();
        if (Optional.ofNullable(imageGroups).isPresent()) {
            return ResponseEntity.ok(imageGroups);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
