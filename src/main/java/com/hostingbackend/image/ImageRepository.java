package com.hostingbackend.image;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

interface ImageRepository {
    List<Image> findAll();

    Page<Image> findAll(Pageable pageable);

    Optional<Image> findById(Integer id);

    boolean existsById(Integer id);

    Image save(Image entity);
}
