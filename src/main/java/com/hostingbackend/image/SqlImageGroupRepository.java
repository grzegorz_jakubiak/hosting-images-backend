package com.hostingbackend.image;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface SqlImageGroupRepository extends ImageGroupRepository, JpaRepository<ImageGroup, Integer> {

    @Override
    @Query("select distinct ig from ImageGroup ig join fetch ig.images")
    List<ImageGroup> findAll();


}
