package com.hostingbackend.image;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface SqlImageRepository extends ImageRepository, JpaRepository<Image, Integer> {
}
