package com.hostingbackend.image;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
class ImageGroupService {

    private final ImageGroupRepository imageGroupRepository;

    ImageGroup createImageGroup(ImageGroup imageGroup){
        return imageGroupRepository.save(imageGroup);
    }

    List<ImageGroup> findAllImageGroups(){
        return imageGroupRepository.findAll();
    }

    void deleteImageGroup(Integer id){
        imageGroupRepository.deleteById(id);
    }

}
